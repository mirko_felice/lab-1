Note: \
Per questo laboratorio non ho potuto/avuto tempo di eseguire i commit come suggerito.

Per il primo modulo, dopo aver creato la classe di test, andando avanti con il TDD, 
ho modellato una nuova interfaccia 'BankAccountWithAtm' la quale estende l'interfaccia di base 
in quanto le azioni richieste sono le stesse in aggiunta alle due azioni tramite ATM.
Dopodiché, ho creato la classe che implementasse la nuova interfaccia, accorgendomi in seguito che si potesse operare un refactoring.
Ho infatti aggiunto l'estensione della nuova classe alla precedente: in questo modo la nuova classe rispetta il principio DRY.
Infine, per eventuali nuove estensioni di 'BankAccount' ho estratto anche una classe astratta che contenesse già la logica dell''AccountHolder'.
Per questo modulo sono abbastanza soddisfatto, se non per il mancato refactoring delle classi di test.

Per quanto riguarda il secondo modulo, ho implementato la 'CircularList' tramite una lista e un 'ListIterator' il quale offre già metodi quali next() e previous().
Per gestire la ciclicità semplicemente creo un nuovo iteratore quando necessario.
Ho poi implementato le diverse tipologie di 'Strategy' e infine applicato il pattern 'AbstractFactory'.
Sono soddisfatto del risultato, anche qui a eccezione dei test, magari da migliorare.
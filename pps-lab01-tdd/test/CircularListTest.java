import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private final SelectStrategyFactory strategyFactory = new SelectStrategyFactoryImpl();
    private CircularList circularList;

    @BeforeEach
    public void setUp() {
        this.circularList = new CircularListImpl();
        testIsEmpty();
    }

    @Test
    public void testAdd() {
        this.circularList.add(0);
        assertEquals(1, circularList.size());
    }

    @Test
    public void testIsEmpty() {
        assertTrue(circularList.isEmpty());
    }

    @Test
    public void testNext() {
        circularList.add(1);
        Optional<Integer> next = circularList.next();
        assertTrue(next.isPresent());
        assertEquals(1, next.get());
    }

    @Test
    public void testPrevious() {
        circularList.add(1);
        circularList.add(2);
        Optional<Integer> previous = circularList.previous();
        assertTrue(previous.isPresent());
        assertEquals(2, previous.get());
    }

    @Test
    public void testReset() {
        for (int i = 0; i < 10; i++) {
            circularList.add(i);
        }
        for (int i = 0; i < 5; i++) {
            circularList.next();
        }
        circularList.reset();
        Optional<Integer> next = circularList.next();
        assertTrue(next.isPresent());
        assertEquals(0, next.get());
    }

    @Test
    public void testIsActuallyCircular() {
        for (int i = 1; i <= 10; i++) {
            circularList.add(i);
        }
        for (int i = 1; i <= 10; i++) {
            circularList.next();
        }
        Optional<Integer> next = circularList.next();
        assertTrue(next.isPresent());
        assertEquals(1, next.get());
    }

    @Test
    public void testNextEvenStrategy() {
        for (int i = 1; i <= 10; i++) {
            circularList.add(i);
        }
        circularList.next();
        SelectStrategy strategy = strategyFactory.createEvenStrategy();
        for (int i = 1; i <= 10; i+=2) {
            Optional<Integer> next = circularList.next(strategy);
            assertTrue(next.isPresent());
            assertEquals(i+1, next.get());
        }
    }

    @Test
    public void testNextMultipleOfStrategy() {
        for (int i = 1; i <= 10; i++) {
            circularList.add(i);
        }
        SelectStrategy strategy = strategyFactory.createMultipleOfStrategy(3);
        for (int i = 3; i <= 10; i+=3) {
            Optional<Integer> next = circularList.next(strategy);
            assertTrue(next.isPresent());
            assertEquals(i, next.get());
        }
    }

    @Test
    public void testNextEqualsStrategy() {
        for (int i = 1; i <= 10; i++) {
            circularList.add(i);
        }
        circularList.previous();
        SelectStrategy strategy = strategyFactory.createEqualsStrategy(3);
        Optional<Integer> next = circularList.next(strategy);
        assertTrue(next.isPresent());
        assertEquals(3, next.get());
    }

    @Test
    public void testNextStrategyIsEmpty() {
        for (int i = 1; i <= 10; i++) {
            circularList.add(i);
        }
        SelectStrategy strategy = strategyFactory.createEqualsStrategy(15);
        Optional<Integer> next = circularList.next(strategy);
        assertFalse(next.isPresent());
    }

}

package lab01.tdd;

public class EqualsStrategy implements SelectStrategy {

    private final int element;

    public EqualsStrategy(int element) {
        this.element = element;
    }

    @Override
    public boolean apply(int element) {
        return this.element == element;
    }
}

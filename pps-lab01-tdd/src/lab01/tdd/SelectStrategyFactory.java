package lab01.tdd;

public interface SelectStrategyFactory {

    SelectStrategy createEvenStrategy();

    SelectStrategy createMultipleOfStrategy(int element);

    SelectStrategy createEqualsStrategy(int element);

}

package lab01.tdd;

import java.util.*;

public class CircularListImpl implements CircularList {

    private final List<Integer> list = new ArrayList<>();
    private ListIterator<Integer> iterator;

    @Override
    public void add(int element) {
        list.add(element);
        reset();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (!iterator.hasNext())
            reset();
        return Optional.of(iterator.next());
    }

    @Override
    public Optional<Integer> previous() {
        if (!iterator.hasPrevious())
            iterator = list.listIterator(list.size());
        return Optional.of(iterator.previous());
    }

    @Override
    public void reset() {
        iterator = list.listIterator();
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        int index = iterator.nextIndex();
        while (iterator.hasNext()){
            int element = iterator.next();
            if (strategy.apply(element))
                return Optional.of(element);
        }
        reset();
        while (iterator.nextIndex() != index){
            int element = iterator.next();
            if (strategy.apply(element))
                return Optional.of(element);
        }
        return Optional.empty();
    }
}

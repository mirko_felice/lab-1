package lab01.tdd;

public class MultipleOfStrategy implements SelectStrategy {

    private final int element;

    public MultipleOfStrategy(int element) {
        this.element = element;
    }

    @Override
    public boolean apply(int element) {
        return element % this.element == 0;
    }
}

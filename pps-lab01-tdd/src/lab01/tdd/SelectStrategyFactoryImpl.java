package lab01.tdd;

public class SelectStrategyFactoryImpl implements SelectStrategyFactory {

    @Override
    public SelectStrategy createEvenStrategy() {
        return new EvenStrategy();
    }

    @Override
    public SelectStrategy createMultipleOfStrategy(int element) {
        return new MultipleOfStrategy(element);
    }

    @Override
    public SelectStrategy createEqualsStrategy(int element) {
        return new EqualsStrategy(element);
    }
}

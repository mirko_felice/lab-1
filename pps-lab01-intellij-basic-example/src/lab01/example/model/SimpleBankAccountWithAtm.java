package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    private static final int TRANSACTION_FEE = 1;

    public SimpleBankAccountWithAtm(final AccountHolder holder, final double balance) {
        super(holder, balance);
    }

    @Override
    public void depositWithAtm(int usrID, double amount) {
        deposit(usrID, amount - TRANSACTION_FEE);
    }

    @Override
    public void withdrawWithAtm(int usrID, double amount) {
        if (isWithdrawWithAtmAllowed(amount))
            withdraw(usrID, amount + TRANSACTION_FEE);
    }

    private boolean isWithdrawWithAtmAllowed(double amount) {
        return getBalance() >= amount + TRANSACTION_FEE;
    }

}

package lab01.example.model;

public abstract class AbstractBankAccount implements BankAccount {

    private final AccountHolder holder;

    public AbstractBankAccount(final AccountHolder holder) {
        this.holder = holder;
    }

    @Override
    public AccountHolder getHolder() {
        return this.holder;
    }

    protected boolean checkUser(final int id) {
        return this.holder.getId() == id;
    }

}
